﻿
namespace Customer.Service.Models
{
    public class CustomerModel
    {
        public int ID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string Address { get; set; }
        public string Clues { get; set; }
        public string Premise { get; set; }
        public string Comments { get; set; }
    }
}

﻿using Customer.Service.Models;
using System.Collections.Generic;

namespace Customer.Service.Services
{
    public interface ICustomerService
    {
        IEnumerable<ActiveEvent> GetActiveEvents();
        List<ActiveEventCustomerCall> GetActiveEventsById(int eventID, int offset, int limit);
        ActiveEventCustomerCall GetActiveEventById(int eventID);
        IEnumerable<string> GetTroubleCodesByEventId(int eventID);
    }
}

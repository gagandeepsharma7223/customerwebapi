﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Service.Utilities
{
    public static class EnumHelper<T>
    {
        public static string GetEnumDescription(string value)
        {
            Type type = typeof(T);
            var name = Enum.GetNames(type).Where(f => f.Equals(value, StringComparison.CurrentCultureIgnoreCase)).Select(d => d).FirstOrDefault();

            if (name == null)
            {
                return string.Empty;
            }
            var field = type.GetField(name);
            var customAttribute = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return customAttribute.Length > 0 ? ((DescriptionAttribute)customAttribute[0]).Description : name;
        }
    }
    public enum SiteSafetyCodeEnum
    {
        [Description("SHOCK - ANIMAL")]
        AS,
        [Description("CUT AT POLE")]
        CT,
        [Description("FLOOD CUT")]
        FC,
        [Description("FIRE/POLICE")]
        FD,
        [Description("FIRE CUT")]
        FI,
        [Description("SHOCK - HUMAN")]
        HS,
        [Description("POLE ON FIRE")]
        PF,
        [Description("SAFETY CUT - ELECTRIC")]
        S1,
        [Description("SPARKING BLDG")]
        SB,
        [Description("SPARKING GRND")]
        SG,
        [Description("SPARK AT POLE")]
        SP,
        [Description("STRAY VOLTAGE RPT")]
        VS,
        [Description("MUNI REQUEST")]
        W,
        [Description("WIRE BURNING")]
        WB,
        [Description("WIRE ON GROUND")]
        WG,
        [Description("WIRE BLOCK ROAD")]
        WR,
        [Description("METER SHOP REQUEST")]
        X,
        [Description("THEFT FOUND")]
        Y,
        [Description("TREE TAKE WIRE DOWN")]
        TW,
        [Description("TREE SPARK WIRE")]
        TS,
        [Description("TREE POLE-POLE")]
        TP,
        [Description("TREE POLE-HOUSE")]
        TH,
        [Description("TREE LYING ON WIRE")]
        TL,
        [Description("TREE LIMB ON WIRE")]
        TI,
        [Description("POLE BROKEN")]
        PB,
        [Description("POLE LEANING")]
        PL
    }
}
